/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scadarmi_interface;

import java.rmi.Remote;
import java.rmi.RemoteException;
import javafish.clients.opc.component.OpcGroup;
import javafish.clients.opc.component.OpcItem;

/**
 *
 * @author Administrador
 */
public interface scadaRMI_interface extends Remote{
    
        
    public boolean ConnectOPCServer(String host,String serverID)
            throws RemoteException;
    
    public boolean DesconnectOPCServer()
            throws RemoteException;
    
    public Double[] read_tag() 
            throws RemoteException;
    
    public void write_tag(OpcItem tag, double value)
            throws RemoteException;
 
    
}
